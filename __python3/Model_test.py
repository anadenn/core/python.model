import unittest
from dataModel.models import Model,Field

class MyObject(Model):

    B=Field(default=1)

class Test_Model(unittest.TestCase):
    def setUp(self):
        self.node = MyObject(a=1)

    def test_variables(self):
        #lire la variable
        self.assertIn("a",self.node.keys())
        self.assertEqual(self.node.a, 1)
        self.assertEqual(self.node["a"], 1)

        #modifier la variable
        self.node.a=2
        self.assertEqual(self.node.a, 2)
        self.assertEqual(self.node["a"], 2)

        #detruire la variable
        del self.node.a
        self.assertNotIn("a",self.node.keys())
        with self.assertRaises(AttributeError):
            self.assertEqual(self.node.a, 2)


    def test_field_variables(self):
        #lire la variable
        self.assertIn("b",self.node.keys())
        self.assertEqual(self.node.b, 1)
        self.assertEqual(self.node["b"], 1)

        #modifier la variable
        self.node.b=2
        self.assertEqual(self.node.b, 2)
        self.assertEqual(self.node["b"], 2)

        #detruire la variable
        del self.node.b
        # on ne peut pas détruire une variable Field
        self.assertIn("b",self.node.keys())
        self.assertEqual(self.node.b, 2)




if __name__ == '__main__':
    unittest.main()
