import unittest
from dataModel.models import Model,Field
from dataModel.structures.tree import NamedTree
class MyObject(Model,NamedTree):

    B=Field(default=1)

class Test_Tree(unittest.TestCase):

    def setUp(self):
        self.node = MyObject(a=1)
        for i in range(10):
            MyObject(parent=self.node,name=str(i))

    def test_children(self):
        self.assertEqual(len(self.node.children), 10)
        self.assertEqual(self.node.find("/"), self.node)
        self.assertEqual(self.node.find("."), self.node)
        self.assertEqual(self.node.find(""), self.node)

        test_node=self.node.find('/3')
        self.assertEqual(test_node.name,"3")
        self.assertIn(test_node,self.node.children)
        test_node.destroy()
        self.assertNotIn(test_node,self.node.children)
        del self.node.a
        self.assertEqual(self.node.find('/3'), None)


class Test_Tree2(unittest.TestCase):

    def build_node(self,node,level,size):
        if level==0:
            return

        for i in range(size):
            elt=MyObject(parent=node,name=str(i))
            self.build_node(elt,level-1,size)


    def setUp(self):
        self.node = MyObject(a=1)
        self.build_node(self.node,3,3)

    def test_recursive(self):
        self.assertEqual(len(self.node.children), 3)
        self.assertEqual(self.node.deph, 3)
        self.assertEqual(len(self.node.descendants), 39)
        self.assertEqual(len(self.node.leaves), 27)


if __name__ == '__main__':
    unittest.main()
