from .instances.Method import Method
import console

#=============================================================================

class Function(object):

#=============================================================================

    """

    """

    #--------------------------------------------------------------------------

    def __init__(self,*decorators,**attributes):

        self.name=None
        self.doc=None
        self.decorators=decorators
        self.attributes=attributes
        self.function=None
    #--------------------------------------------------------------------------
    def setFunction(self,node):
        if hasattr(node,self.name):
            self.function=getattr(node,self.name)
            obj=Method(self,self.function,node)
            setattr(node,self.name,obj)

        else:
            console.error(text="XXX ERROR no function in model ",function=self.name,model=node.__class__.__name__)


    #--------------------------------------------------------------------------
    def check_args(self,node,function,*args,**kwargs):

        "check arguments and return errors"

        errors=list()

        if len(self.attributes.items())>0:

            for name,attr in self.attributes.items():

                if name in kwargs.keys():
                    kwargs[name]=attr.get_value(kwargs[name])
                elif attr.default:
                    kwargs[name]=attr.get_value(None)
                else:
                    errors.append(dict(name=name,text="no value for attribute"))


        if len(errors)>0:
            console.error(text="error when execute",errors=errors)
            raise Exception("ERROR "+node.__class__.__name__+" in function "+self.name)
        return kwargs

    #--------------------------------------------------------------------------
    def execute(self,node,function,*args,**kwargs):

        kwargs=self.check_args(node,function,*args,**kwargs)

        for decorator in self.decorators:

            decorator.init(node,function)
            function=decorator

        return function(*args,**kwargs)
    #--------------------------------------------------------------------------

    def __help__(self):

        result=dict()

        result[ "doc" ] = self.doc
        attrs=dict()
        for k,v in self.attributes.items():
            attrs[k]=v.__help__(funct=True)

        decorators=list()
        for k in self.decorators:
            decorators.append(k.__help__())

        if len(attrs)>0:
            result[ "attrs" ]=attrs
        if len(decorators)>0:
            result[ "decorators" ]=decorators

        return result            
    #--------------------------------------------------------------------------

#=============================================================================

