from .Decorator import Decorator
from ..Frame import Frame

#=============================================================================

class ReturnFrame(Decorator):

#=============================================================================


    def onCall(self,function,args,kwargs,Class=Frame,**options):
        node=Class()
        for elt in function(*args,**kwargs):
            node.append(elt)
        return node


#=============================================================================
