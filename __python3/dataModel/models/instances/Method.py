#=============================================================================

class Method(object):

#=============================================================================
    """
    link between decorator and function
    create when function is assigned
    """
    def __init__(self,obj,function,node):
        self.obj=obj
        self.function=function
        self.node=node
        self.name=None

    def __call__(self,*args,**kwargs):
        #print("interf",self.obj.name,args,kwargs)
        return self.obj.execute(self.node,self.function,*args,**kwargs)


#=============================================================================
