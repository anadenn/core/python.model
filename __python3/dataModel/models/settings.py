


ALL_CLASSES=dict()



#=============================================================================

def help():

#=============================================================================
    """

    """

    result=dict()
    for k,v in ALL_CLASSES.items():
        result[k]=v.__help__()
    return result

#=============================================================================

def models():

#=============================================================================
    """
    renvoi la liste des classes ::

       for cls in pytree.models():
           ...

    renvoyer les champs ::

       for cls in pytree.models():
           for attr in self._fields.values():
               ...

    """

    return ALL_CLASSES.values()


#===============================================================

def concretes():

#===============================================================
    """
    retourne un dictionnaire de classes concrètes
    les classes concrètes héritent de Model
    """
    r=dict()
    for name,cls in ALL_CLASSES.items():
        if issubclass(cls,Model):
            r[name]=cls
    return r
#===============================================================

def abstracts():

#===============================================================
    """
    retourne un dictionnaire de classes abstraites
    les classes concrètes n'héritent pas de Model
    """
    from ..model import Model
    r=dict()
    for name,cls in ALL_CLASSES.items():
        if not issubclass(cls,Model):
            r[name]=cls
    return r

#===============================================================

def getClass(Class):

#===============================================================
    """
    renvoie la classe à partir du nom
    permet de tester et convertir une classe à la volée :
        cls1=getClass("name")
        cls2=getClass(cls1)
        print(cls1==cls2)

    renvoie une exception si la classe n'est pas trouvée
    """

    if type(Class) == str:

        if Class in ALL_CLASSES.keys():
            return ALL_CLASSES[Class]
        else:
            raise Exception("classe not found : %s"%Class)
    else:
        return Class

#===============================================================
