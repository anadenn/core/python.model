from dataModel.models import *
from .decorators import *
#=======================================================================

class LinkStore(Frame):

#=======================================================================

    #STORE
    #=========================================================

    #----------------------------------------------------------
    @links
    def by_class(self,cls):
        return Frame.by_class(self,cls)

    #----------------------------------------------------------
    @links
    def by_attribute(self,attribute):
        return Frame.by_attribute(self,attribute)

    #----------------------------------------------------------


    #LINKS
    #=========================================================

    #----------------------------------------------------------
    @links
    def selfLoops(self):
        for elt in self:
            if elt.is_self_loop():
                yield elt

    #----------------------------------------------------------

    #NODES
    #=========================================================

    #INTERNALS

    #----------------------------------------------------------
    @nodes
    def all_nodes(self):
        return self.sources().union(self.targets())
    #----------------------------------------------------------
    @nodes
    def sources(self):
        for elt in self:
            yield elt.source

    #----------------------------------------------------------
    @nodes
    def targets(self):
        for elt in self:
            yield elt.target


    #----------------------------------------------------------

    #INTERNALS FLOW

    #----------------------------------------------------------
    @nodes
    def upstreams(self):
        return self.sources()-self.internals()

    #----------------------------------------------------------
    @nodes
    def downstreams(self):
        return self.targets()-self.internals()

    #----------------------------------------------------------

    @nodes
    def borders(self):
        return self.downstreams()+self.upstreams()

    #----------------------------------------------------------
    @nodes
    def internals(self):

        return self.sources().intersection(self.targets())

    #----------------------------------------------------------

    #EXTERNALS
    #----------------------------------------------------------

    #=========================================================

